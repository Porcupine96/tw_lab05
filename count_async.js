const _ = require('lodash/fp');
const fs = require('fs');
const file = require('file');
const async = require('async');
const { promisify } = require('util');

let total = 0;
let openTasks = 0;

const path = './PAM08';

function countForFileAsync(filePath, callback) {
  let count = 0;
  fs
    .createReadStream(filePath)
    .on('data', chunk => {
      // prettier-ignore
      count +=
        chunk
          .toString('utf8')
          .split(/\r\n|[\n\r\u0085\u2028\u2029]/g)
          .length - 1;
    })
    .on('end', () => {
      total += count;
      openTasks -= 1;
      // console.log(filePath, count);
      openTasks === 0 ? callback(null, 'done') : callback(null, 'progress');
    })
    .on('error', err => {
      callback(err);
    });
}

function asyncCount(path, callback) {
  file.walk(path, (err, dirPath, dirs, files) => {
    openTasks += files.length;
    async.parallel(_.map(f => cb => countForFileAsync(f, cb), files), callback);
  });
}

asyncCount(path, (err, res) => {
  if (err) console.error('error', err);
  else if (_.includes('done', res)) console.log('success:', total);
});
