const _ = require('lodash/fp');
const fs = require('fs');
const file = require('file');
const async = require('async');
const { promisify } = require('util');

const path = './PAM08';

let total = 0;
let openTasks = 0;

const BASE_CALLBACK = cb => cb(null, 0);

function countForFileSync(filePath, currentCount, callback) {
  let count = 0;
  fs
    .createReadStream(filePath)
    .on('data', chunk => {
      // prettier-ignore
      count +=
        chunk
          .toString('utf8')
          .split(/\r\n|[\n\r\u0085\u2028\u2029]/g)
          .length - 1;
    })
    .on('end', () => {
      console.log(filePath, count);
      callback(null, currentCount + count);
    })
    .on('error', err => {
      callback(err);
    });
}

function countForDirectory(path, currentCount, callback) {
  openTasks += 1;
  file.walkSync(path, (dirPath, dirs, files) => {
    const absPath = f => file.path.join(dirPath, f);

    openTasks += dirs.length;

    const countLinesCallbacks = _.map(
      f => (acc, cb) => countForFileSync(absPath(f), acc, cb),
      files
    );

    async.waterfall([BASE_CALLBACK, ...countLinesCallbacks], (err, result) => {
      if (err) callback(err);
      else {
        openTasks -= 1;
        total += result;
        if (openTasks === 0) callback(null, total);
      }
    });
  });
}

function syncCount(path, callback) {
  countForDirectory(path, 0, callback);
}

syncCount(path, (err, res) => {
  if (err) console.error('error:', err);
  else console.log('success:', res);
});
