const async = require('async');

function printAsync(s, cb) {
  var delay = Math.floor(Math.random() * 1000 + 500);
  setTimeout(function() {
    console.log(s);
    if (cb) cb();
  }, delay);
}

function task1(cb) {
  printAsync('1', function() {
    task2(cb);
  });
}

function task2(cb) {
  printAsync('2', function() {
    task3(cb);
  });
}

function task3(cb) {
  printAsync('3', cb);
}

function loop(n) {
  if (n === 0) console.log('done');
  else task1(() => loop(n - 1));
}

function loopWaterfall(n) {
  if (n === 0) console.log('done');
  else {
    async.waterfall(
      [
        cb => printAsync('1', cb),
        cb => printAsync('2', cb),
        cb => printAsync('3', cb)
      ],
      err => {
        if (err) console.error(err);
        else loopWaterfall(n - 1);
      }
    );
  }
}

loopWaterfall(4);
